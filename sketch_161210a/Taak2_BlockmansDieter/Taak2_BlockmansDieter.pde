//Dieter Bockmans
//dieter.blockmans@gmail.com  
//Multimedia en communicatietechnologie
//2016-2017
//Muziek: WISHI, Toerist

//minim, Beatdetect
//http://code.compartmental.net/tools/minim/quickstart/
//geraadpleegd op 10 december 2016
import ddf.minim.*;
import ddf.minim.analysis.*;
Minim minim;
AudioPlayer song;
BeatDetect beat;

float Ellipse = 100; //word gebruikt in void drawEllipse
boolean terugkeerX = true; // word gebruikt in void drawLijnen2
boolean terugkeerY = true; //word gebruikt in void drawLijnen2
int lineX = 0; //word gebruikt in void drawLijnen2, horizontaal
int lineY = 0; //word gebruikt in void drawLijnen2, verticaal
int punten = 1000; // aantal gekleurde bolletjes in drawCirkels
int[] puntX = new int [punten]; //aantal bolletje hun X waarde om verticaal te bewegen
int[] puntY = new int [punten]; //aantal bolletje hun Y waarde om verticaal te bewegen
color donker = color(random(40, 100), random(20,50), random(20,50));
color kleur = color(random(101, 360), random(50, 100), random(50, 100));

void setup() {
  size(1280, 720);
  minim = new Minim(this);
  song = minim.loadFile("song.mp3", 2048);
  song.play();
  beat = new BeatDetect();
  colorMode(HSB, 360, 100, 100);

  //deze funcie is voor de bolletjes op en naar te bewegen in drawCirkels
  for (int i = 0; i<punten; i++) {
    puntY[i] = round(random(0, height));
  }
}

void draw() {
  background(0);

  drawLijnen1();
  drawEllipse();
  drawLijnen2();
  drawCirkels();
}

void drawLijnen1() {
  if (key == '1') {
    stroke(255);
    for (int i=0; i< song.bufferSize() -1; i++) {
      strokeWeight(7);
      stroke(255);
      line(i, (height/2-100)+ song.mix.get(i)*150, i+1, (height/2-100)+ song.mix.get(i+1)*150); //1ste horizontale lijn
      line(i, (height/2+100)+ song.mix.get(i)*150, i+1, (height/2+100)+ song.mix.get(i+1)*150); //2de horizontale lijn
      strokeWeight(2);
      line(width/4 + song.left.get(i)*100, i, width/4 + song.left.get(i)*100, i+1); // verticale lijn links
      line(width/2+ song.right.get(i)*100, i, width/2+ song.mix.get(i)*100, i+1);// verticale lijn midden
      line(width/4+(width/2)+ song.right.get(i)*100, i, width/4+(width/2)+song.right.get(i)*100, i+1);//verticale lijn rechts
    }
  }
}

void drawEllipse() {
  if (key == '2') {
    beat.detect(song.mix);
    if (beat.isOnset()) Ellipse = 250; 
    {  
      noFill();
      ellipse( width/2, height/2, Ellipse, Ellipse); //cirkel midden
      ellipse( width/2, height/2, Ellipse+50, Ellipse+50); //2de cirkel midden
      ellipse( width/2, height/2, Ellipse+100, Ellipse+100); // 3de cirkel midden
      ellipse( width/2, height/2, Ellipse+200, Ellipse+200); // 4de cirkels midden
      Ellipse *=0.5; 
      fill(random(0, 200));
      ellipse(0, 0, Ellipse+150, Ellipse+150); //cirkel linksboven
      ellipse(width, height, Ellipse+100, Ellipse+100); // cirkel rechtsbeneden
      Ellipse *= 2.5;
    }
  }
}

void drawLijnen2() {
  if (key == '3') {
    for (int i=0; i< song.bufferSize(); i++) {
      strokeWeight(3);
      stroke(255);
      line(lineX +song.mix.get(i)*50, i, lineX+song.mix.get(i)*50, i+1); //horizontale lijn
      line((lineX+100) +song.mix.get(i)*50, i, (lineX+100)+song.mix.get(i)*50, i+1); //horizontale lijn
      line((lineX+200) +song.mix.get(i)*50, i, (lineX+200)+song.mix.get(i)*50, i+1); // horizontale lijn
      line(i, lineY+song.mix.get(i)*50, i+1, lineY+song.mix.get(i)*50);
      line(i, (lineY+100)+song.mix.get(i)*50, i+1, (lineY+100)+song.mix.get(i)*50);
      line(i, (lineY+200)+song.mix.get(i)*50, i+1, (lineY+200)+song.mix.get(i)*50);
    }
    if ((lineX+200)>width) {
      terugkeerX = false;
    } else if (lineX<0) {
      terugkeerX = true;
    }
    if ((lineY+200)>height) {
      terugkeerY = false;
    } else if (lineY<0) {
      terugkeerY = true;
    }
    if (terugkeerX == true) {
      lineX+=10;
    } else {
      lineX-=10;
    }
    if (terugkeerY == true) {
      lineY+=10;
    } else {
      lineY-=10;
    }
  }
}

void drawCirkels() {
  if (key == '4') {
    beat.detect(song.mix);
    stroke(0);
    fill(donker);
    for ( int i = 0; i<700; i+=100) {
      ellipse(width/2, height/2, 600-i, 600-i);
    }
    if (beat.isOnset()) {
      fill(kleur);
      for ( int i = 0; i<700; i+=20) {
        stroke(0);
        ellipse(width/2, height/2, 600-i, 600-i);
      }
      for (int i = 0; i < punten; i++) {
        fill(0);
        ellipse(i*width/ (punten -1), puntY[i], 3, 3);
        puntX[i] += round(random(-3, 3));
        puntY[i] += round(random(-3, 3));
      }
    }
  }
}